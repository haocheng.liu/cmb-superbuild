cmake_minimum_required(VERSION 3.8.2)

project(cmb-superbuild)

list(INSERT CMAKE_MODULE_PATH 0
  "${CMAKE_CURRENT_LIST_DIR}/cmake")

macro (superbuild_setup_variables)
  include(SuperbuildVersionMacros)
  if (cmb_SOURCE_SELECTION STREQUAL "git")
    # Assuming master; just use the latest version, but let the user set their
    # own version in case it is actually a branch from some other version.
    set(CMB_VERSION_DEFAULT "5.0.0-develop"
      CACHE STRING "The default version of CMB to use if it cannot be detected")
    mark_as_advanced(CMB_VERSION_DEFAULT)
    set(cmb_default_version "${CMB_VERSION_DEFAULT}")
  elseif (cmb_SOURCE_SELECTION STREQUAL "source")
    # If it is a git repo, we'll get it that way, otherwise we will look at the
    # `version.txt` in the checkout.
    set(cmb_default_version "")
  else ()
    # The selection is the version number; use it.
    set(cmb_default_version "${cmb_SOURCE_SELECTION}")
  endif()
  superbuild_set_version_variables(cmb "${cmb_default_version}" "cmb-version.cmake" "version.txt")
  set(cmb_version "${cmb_version_major}.${cmb_version_minor}")
  superbuild_set_version_variables(paraview "5.4.0-RC3" "paraview-version.cmake" "version.txt")
  set(paraview_version "${paraview_version_major}.${paraview_version_minor}")
  superbuild_set_version_variables(vtk "8.1.0" "vtk-version.cmake")
  set(vtk_version "${vtk_version_major}.${vtk_version_minor}")
endmacro ()

function (superbuild_find_projects var)
  include(CMBSuperbuildMacros)

  set(projects
    boost
    bzip2
    cgm
    cmb
    cmbusersguide
    cmbworkflows
    cumulus
    cxx11
    fortran
    freetype
    ftgl
    gdal
    hdf5
    lapack
    matplotlib
    moab
    netcdf
    nlohmannjson
    numpy
    oce
    opencv
    paraview
    paraviewweb
    paraviewwebvisualizer
    paraviewweblightviz
    png
    pybind11
    python
    pythongirderclient
    pythonrequests
    pythonsetuptools
    pythonsix
    qt
    qt4
    qt5
    remus
    shiboken
    smtk
    smtkusersguide
    szip
    vtk
    vtkonly
    vxl
    zeromq
    zlib)

  if (NOT WIN32)
    list(APPEND projects
      fontconfig
      libxml2)
  endif ()

  if (WIN32)
    list(APPEND projects
      ftjam
      pythonsetuptools
      pywin32)
  endif ()

  option(USE_NONFREE_COMPONENTS "Enable the non-free parts of the superbuild" OFF)
  mark_as_advanced(USE_NONFREE_COMPONENTS)
  if (USE_NONFREE_COMPONENTS)
    list(APPEND projects
      triangle)
  endif ()

  set("${var}"
    ${projects}
    PARENT_SCOPE)
endfunction ()

function (superbuild_sanity_check)
  if (vtkonly_enabled AND paraview_enabled)
    message(FATAL_ERROR
      "Only one of the following projects should be enabled: vtkonly, paraview.")
  endif ()
  if (NOT smtk_enabled AND NOT cmb_enabled)
    message(FATAL_ERROR
      "At least one of the following projects must be enabled: cmb, smtk.")
  endif ()
  if (smtk_SOURCE_SELECTION STREQUAL "from_cmb" AND NOT DEVELOPER_MODE_smtk AND
      (NOT cmb_enabled OR DEVELOPER_MODE_cmb))
    message(FATAL_ERROR
      "SMTK cannot use CMB's SMTK submodule without CMB being enabled and built by the superbuild.")
  endif ()
  if (shiboken_enabled AND pybind11_enabled)
    message(FATAL_ERROR
      "Only one python wrapping protocol should be enabled: pybind11, shiboken.")
  endif ()
  if (cmb_enabled AND NOT shiboken_enabled AND NOT pybind11_enabled)
    message(FATAL_ERROR
      "Exactly one python wrapping protocol must be enabled: pybind11, shiboken.")
  endif ()
  if (shiboken_enabled AND NOT ENABLE_qt4)
    message(FATAL_ERROR
      "Shiboken requires qt4; either use qt4 + shiboken or qt5 + pybind11.")
  endif ()
endfunction ()

function (superbuild_add_packaging)
  if (NOT cmb_enabled OR DEVELOPER_MODE_cmb)
    return ()
  endif ()

  if (WIN32)
    set(generators
      ZIP)
  elseif (APPLE)
    set(generators
      DragNDrop)
  else ()
    set(generators
      TGZ)
  endif ()

  if (USE_SYSTEM_qt4)
    list(APPEND superbuild_export_variables
      QT_LIBRARY_DIR)
  endif ()

  if (qt5_built_by_superbuild)
    set(Qt5_DIR "${superbuild_install_location}/lib/cmake/Qt5")
  endif ()
  if (USE_SYSTEM_qt5)
    list(APPEND superbuild_export_variables
      Qt5_DIR)
  endif ()

  include(cmb.suffix)
  list(APPEND superbuild_export_variables
    CMB_PACKAGE_SUFFIX)

  if (cmb_enabled)
    set(CMB_PACKAGE_FILE_NAME ""
      CACHE STRING "If set, overrides the generated filename for the package")
    list(APPEND superbuild_export_variables
      CMB_PACKAGE_FILE_NAME)
  endif ()

  option(cmb_ENABLE_HYDROLOGY_PACKAGE "Build full hydrology package" ON)

  foreach (generator IN LISTS generators)
    superbuild_add_extra_package_test(modelbuilder "${generator}"
      LABELS  "CMB"
      TIMEOUT 6400)

    if (cmb_ENABLE_HYDROLOGY_PACKAGE)
      superbuild_add_extra_package_test(hydrology "${generator}"
        LABELS  "CMB"
        TIMEOUT 6400)
    endif()
  endforeach ()
endfunction ()

macro (superbuild_setup_tests)
  if (cmb_enabled)
    set(DROP_LOCATION "/submit.php?project=CMB")
  elseif (smtk_enabled)
    set(DROP_LOCATION "/submit.php?project=SMTK")
  endif ()
endmacro ()

function (superbuild_add_tests)
  set_property(GLOBAL APPEND
    PROPERTY
      superbuild_test_projects cmb smtk)
endfunction ()

set(CMB_TEST_DATA_ROOT "" CACHE PATH "Location of CMB's testing data")

# Default Qt5 to ON.
set(_superbuild_default_qt5 ON)

set(boost_minimum_version 1.64.0)
set(boost_libraries
  atomic
  chrono
  date_time
  filesystem
  log
  regex
  serialization
  system
  thread
  timer)

configure_file(
  "${CMAKE_CURRENT_LIST_DIR}/cmake/CTestCustom.cmake.in"
  "${CMAKE_CURRENT_BINARY_DIR}/CTestCustom-cmb.cmake"
  @ONLY)

if (__BUILDBOT_INSTALL_LOCATION)
  set(superbuild_install_location "${__BUILDBOT_INSTALL_LOCATION}")
endif ()

set(suppress_projects
  boost
  cgm
  freetype
  ftgl
  gdal
  hdf5
  netcdf
  numpy
  oce
  png
  pybind11
  shiboken
  szip
  vxl
  zeromq
  zlib)
foreach (suppress_project IN LISTS suppress_projects)
  set("_superbuild_suppress_${suppress_project}_output" ON)
endforeach ()

list(APPEND superbuild_version_files
  "${CMAKE_CURRENT_LIST_DIR}/versions.cmake")
list(APPEND superbuild_ctest_custom_files
  "${CMAKE_CURRENT_BINARY_DIR}/CTestCustom-cmb.cmake")
list(APPEND superbuild_project_roots
  "${CMAKE_CURRENT_LIST_DIR}/projects")
add_subdirectory(superbuild)

if (TARGET "smtk" AND NOT DEVELOPER_MODE_smtk AND smtk_SOURCE_SELECTION STREQUAL "from_cmb")
  get_property(smtk_need_cmb_download GLOBAL
    PROPERTY smtk_need_cmb_download)
  if (smtk_need_cmb_download)
    add_dependencies("smtk"
      "cmb-download")
  endif ()
endif ()
