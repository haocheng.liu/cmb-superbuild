set(cmb_plugins)
foreach (cmb_plugin IN LISTS cmb_plugins_all)
  if (EXISTS "${superbuild_install_location}/Applications/ModelBuilder.app/Contents/Libraries/lib${cmb_plugin}.dylib")
    list(APPEND cmb_plugins
      ${cmb_plugin})
    set("cmb_plugin_path_${cmb_plugin}"
      "${superbuild_install_location}/Applications/ModelBuilder.app/Contents/Libraries/lib${cmb_plugin}.dylib")
    continue ()
  endif ()

  foreach (path IN ITEMS "" "cmb-${cmb_version}" "paraview-${paraview_version}")
    if (EXISTS "${superbuild_install_location}/lib/${path}/lib${cmb_plugin}.dylib")
      list(APPEND cmb_plugins
        ${cmb_plugin})
      set("cmb_plugin_path_${cmb_plugin}"
        "${superbuild_install_location}/lib/${path}/lib${cmb_plugin}.dylib")
      break ()
    endif ()
  endforeach ()
endforeach ()

foreach(program IN LISTS cmb_programs_to_install)
  set(plugins "${cmb_plugins_${program}}")

  set(plugin_paths)
  foreach (plugin IN LISTS plugins)
    if (NOT DEFINED "cmb_plugin_path_${plugin}")
      message(FATAL_ERROR "The application ${program} needs the ${plugin} plugin, but it was not found.")
    endif ()
    list(APPEND plugin_paths
      "${cmb_plugin_path_${plugin}}")
  endforeach ()

  superbuild_apple_create_app(
    "\${CMAKE_INSTALL_PREFIX}/${cmb_package}"
    "${program}.app"
    "${superbuild_install_location}/Applications/${program}.app/Contents/MacOS/${program}"
    CLEAN
    FAKE_PLUGIN_PATHS
    PLUGINS ${plugin_paths}
    SEARCH_DIRECTORIES
            "${superbuild_install_location}/lib")
  foreach (icon_filename MacIcon.icns pvIcon.icns)
    set(icon_path "${superbuild_install_location}/Applications/${program}.app/Contents/Resources/${icon_filename}")
    if (EXISTS "${icon_path}")
      install(
        FILES       "${icon_path}"
        DESTINATION "${cmb_package}/${program}.app/Contents/Resources"
        COMPONENT   superbuild)
    endif ()
  endforeach ()
  install(
    FILES       "${superbuild_install_location}/Applications/${program}.app/Contents/Info.plist"
    DESTINATION "${cmb_package}/${program}.app/Contents"
    COMPONENT   superbuild)

  foreach (executable IN LISTS paraview_executables)
    superbuild_apple_install_utility(
      "\${CMAKE_INSTALL_PREFIX}/${cmb_package}"
      "${program}.app"
      "${superbuild_install_location}/Applications/paraview.app/Contents/bin/${executable}"
      SEARCH_DIRECTORIES
              "${superbuild_install_location}/lib")
  endforeach ()

  install(CODE
    "file(MAKE_DIRECTORY \"\${CMAKE_INSTALL_PREFIX}/${cmb_package}/${program}.app/Contents/Resources\")
    file(WRITE \"\${CMAKE_INSTALL_PREFIX}/${cmb_package}/${program}.app/Contents/Resources/qt.conf\" \"\")"
    COMPONENT superbuild)

  set(plugins_file "${CMAKE_CURRENT_BINARY_DIR}/${program}.plugins")
  cmb_add_plugin("${plugins_file}" ${plugins})

  install(
    FILES       "${plugins_file}"
    DESTINATION "${cmb_package}/${program}.app/Contents/Plugins"
    COMPONENT   superbuild
    RENAME      ".plugins")

  superbuild_apple_install_python(
    "\${CMAKE_INSTALL_PREFIX}/${cmb_package}"
    "${program}.app"
    MODULES ${cmb_python_modules}
    MODULE_DIRECTORIES
            "${superbuild_install_location}/lib/python2.7/site-packages"
            "${superbuild_install_location}/Applications/paraview.app/Contents/Python"
    SEARCH_DIRECTORIES
            "${superbuild_install_location}/lib"
            "${superbuild_install_location}/Applications/paraview.app/Contents/Libraries")

  # Install the paraview.vtk module.
  install(CODE
    "file(INSTALL
    FILES       \"\$ENV{DESTDIR}\${CMAKE_INSTALL_PREFIX}/${cmb_package}/${program}.app/Contents/Python/vtk/\"
    DESTINATION \"\${CMAKE_INSTALL_PREFIX}/${cmb_package}/${program}.app/Contents/Python/paraview/vtk\")"
    COMPONENT   superbuild)

  if (matplotlib_enabled)
    install(
      DIRECTORY   "${superbuild_install_location}/lib/python2.7/site-packages/matplotlib/mpl-data/"
      DESTINATION "${cmb_package}/${program}.app/Contents/Python/matplotlib/mpl-data"
      COMPONENT   superbuild)
  endif ()

  if (pythonrequests_enabled)
    install(
      FILES       "${superbuild_install_location}/lib/python2.7/site-packages/requests/cacert.pem"
      DESTINATION "${cmb_package}/${program}.app/Contents/Python/requests"
      COMPONENT   superbuild)
  endif ()

  if (paraviewweb_enabled)
    install(
      FILES       "${superbuild_install_location}/Applications/paraview.app/Contents/Python/paraview/web/defaultProxies.json"
      DESTINATION "${cmb_package}/Contents/Python/paraview/web"
      COMPONENT   "superbuild")
    install(
      DIRECTORY   "${superbuild_install_location}/share/paraview/web"
      DESTINATION "${cmb_package}/Contents/Resources"
      COMPONENT   "superbuild")
  endif ()

  foreach (qt5_plugin_path IN LISTS qt5_plugin_paths)
    get_filename_component(qt5_plugin_group "${qt5_plugin_path}" DIRECTORY)
    get_filename_component(qt5_plugin_group "${qt5_plugin_group}" NAME)

    superbuild_apple_install_module(
      "\${CMAKE_INSTALL_PREFIX}/${cmb_package}"
      "${program}.app"
      "${qt5_plugin_path}"
      "Contents/Plugins/${qt5_plugin_group}"
      SEARCH_DIRECTORIES  "${library_paths}")
  endforeach ()
endforeach ()

# FIXME: Install inside of each application?
install(
  DIRECTORY   "${superbuild_install_location}/share/cmb/workflows/"
  DESTINATION "Workflows"
  COMPONENT   superbuild)
