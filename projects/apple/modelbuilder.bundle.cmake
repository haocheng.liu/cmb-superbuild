include(modelbuilder.bundle.common)
set(cmb_package "ModelBuilder ${cmb_version_major}.${cmb_version_minor}.${cmb_version_patch}")
set(cmb_doc_dir "${cmb_package}/Contents/doc")

include(cmb.bundle.apple)

# Install PDF guides.
cmb_install_extra_data()
