include("${CMAKE_CURRENT_LIST_DIR}/../oce.cmake")

# Pulled from upstream: https://github.com/tpaviot/oce/commit/cbfdba8762e0a3787c7669977e8883366cf2b3e5
superbuild_apply_patch(oce clock-gettime
  "Fix clock_gettime detection on macOS")
