set(CPACK_PACKAGE_DESCRIPTION_SUMMARY
  "CMB ModelBuilder Application")
set(CPACK_PACKAGE_NAME "CMB")
set(cmb_package_name "ModelBuilder")

set(cmb_programs_to_install
  ModelBuilder
  TemplateEditor)

set(cmb_install_paraview_server FALSE)
set(cmb_install_paraview_python TRUE)

include(cmb.bundle.common)
