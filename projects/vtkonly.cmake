set(vtk_extra_cmake_args)

if (UNIX AND NOT APPLE)
  list(APPEND vtk_extra_cmake_args
    -DCMAKE_INSTALL_RPATH_USE_LINK_PATH:BOOL=FALSE)
endif ()

superbuild_add_project(vtkonly
  DEBUGGABLE
  DEPENDS
    gdal
    png
    zlib
    netcdf
  DEPENDS_OPTIONAL
    cxx11 freetype python qt qt4 qt5
  CMAKE_ARGS
    -DBUILD_SHARED_LIBS:BOOL=ON
    -DBUILD_TESTING:BOOL=OFF
    -DVTK_WRAP_PYTHON:BOOL=${python_enabled}
    -DVTK_USE_MPI:BOOL=${mpi_enabled}
    -DVTK_USE_SYSTEM_HDF5:BOOL=${hdf5_enabled}
    -DHDF5_NO_FIND_PACKAGE_CONFIG_FILE:BOOL=ON
    -DVTK_USE_SYSTEM_NETCDF:BOOL=${netcdf_enabled}
    -DVTK_RENDERING_BACKEND:STRING=OpenGL2
    -DVTK_QT_VERSION:STRING=${qt_version}

    #CMB needs geovis enabled to provide the gdal reader
    -DModule_vtkGeovisCore:BOOL=ON
    -DModule_vtkIOGDAL:BOOL=ON
    -DModule_vtkViewsInfovis:BOOL=ON
    -DModule_vtkRenderingMatplotlib:BOOL=ON
    -DModule_vtkRenderingGL2PSOpenGL2:BOOL=ON
    -DModule_vtkTestingRendering:BOOL=ON
    -DModule_vtkGUISupportQt:BOOL=${qt_enabled}
    -DModule_vtkRenderingQt:BOOL=${qt_enabled}
    -DGDAL_DIR:PATH=<INSTALL_DIR>

    -DModule_vtkIOParallelExodus:BOOL=ON

    # since VTK mangles all the following, I wonder if there's any point in
    # making it use system versions.
    -DVTK_USE_SYSTEM_FREETYPE:BOOL=${freetype_enabled}
#    currently png strips rpaths don't use this in cmb, so don't use this
#    -DVTK_USE_SYSTEM_PNG:BOOL=${png_enabled}
    -DVTK_USE_SYSTEM_ZLIB:BOOL=${zlib_enabled}

    -DCMAKE_INSTALL_NAME_DIR:STRING=<INSTALL_DIR>/lib

    ${vtk_extra_cmake_args})

superbuild_add_extra_cmake_args(
  -DVTK_DIR:PATH=<INSTALL_DIR>/lib/cmake/vtk-${vtk_version})
