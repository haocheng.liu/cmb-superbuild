set(smtk_extra_cmake_args)
if (WIN32)
  # On Windows we expect the Python source for module to be
  # in a different place than Unix builds and in a different
  # place than SMTK would put it by default. Tell SMTK where
  # to install Python source for the smtk module:
  list(APPEND smtk_extra_cmake_args
    "-DSMTK_PYTHON_MODULEDIR:PATH=bin/Lib/site-packages")
endif ()

set(smtk_enable_python_wrapping)
if (shiboken_enabled OR pybind11_enabled)
  set(smtk_enable_python_wrapping ON)
endif ()

set(smtk_enable_vtk OFF)
if (vtkonly_enabled OR paraview_enabled)
  set(smtk_enable_vtk ON)
endif ()

if (UNIX AND NOT APPLE)
  list(APPEND smtk_extra_cmake_args
    -DCMAKE_INSTALL_RPATH_USE_LINK_PATH:BOOL=FALSE)
endif ()

set(smtk_response_file)
if (WIN32)
  # Force response file usage in SMTK. The command line gets way too long on
  # Windows without this. Once VTK_USE_FILE and PARAVIEW_USE_FILE are gone,
  # this can be removed again.
  set(smtk_response_file -DCMAKE_NINJA_FORCE_RESPONSE_FILE:BOOL=ON)
endif ()

get_property(smtk_lfs_steps GLOBAL
  PROPERTY cmb_superbuild_lfs_steps)

#explicitly depend on gdal so we inherit the location of the GDAL library
#which FindGDAL.cmake fails to find, even when given GDAL_DIR.
superbuild_add_project(smtk
  DEVELOPER_MODE
  DEBUGGABLE
  DEFAULT_ON
  INDEPENDENT_STEP_TARGETS ${smtk_lfs_steps} download update
  DEPENDS boost cxx11 hdf5 netcdf nlohmannjson
  DEPENDS_OPTIONAL cgm cumulus gdal moab netcdf opencv paraview pybind11 python
                   matplotlib remus shiboken qt qt4 qt5 vtkonly vxl
  CMAKE_ARGS
    ${smtk_extra_cmake_args}
    -DBUILD_SHARED_LIBS:BOOL=ON
    -DSMTK_ENABLE_OPENCV:BOOL=${opencv_enabled}
    -DSMTK_ENABLE_QT_SUPPORT:BOOL=${qt_enabled}
    -DSMTK_ENABLE_VTK_SUPPORT:BOOL=${smtk_enable_vtk}
    -DSMTK_ENABLE_VXL_SUPPORT:BOOL=${vxl_enabled}
    -DSMTK_ENABLE_PARAVIEW_SUPPORT:BOOL=${paraview_enabled}
    -DSMTK_ENABLE_CGM_SESSION:BOOL=${cgm_enabled}
    -DSMTK_ENABLE_DISCRETE_SESSION:BOOL=${smtk_enable_vtk}
    -DSMTK_ENABLE_EXODUS_SESSION:BOOL=${paraview_enabled}
    -DSMTK_ENABLE_REMOTE_SESSION:BOOL=${remus_enabled}
    -DSMTK_ENABLE_REMUS_SUPPORT:BOOL=${remus_enabled}
    -DSMTK_ENABLE_PYTHON_WRAPPING:BOOL=${smtk_enable_python_wrapping}
    -DSMTK_ENABLE_MATPLOTLIB:BOOL=${matplotlib_enabled}
    -DSMTK_USE_PYBIND11:BOOL=${pybind11_enabled}
    -DPYBIND11_INSTALL:BOOL=${pybind11_enabled}
    -DSMTK_QT_VERSION:STRING=${qt_version}
    -DSMTK_DATA_DIR:PATH=${CMB_TEST_DATA_ROOT}

    # This should be off by default because vtkCmbMoabReader in discrete
    # session may only be needed for debugging purpose
    -DSMTK_ENABLE_MOAB_DISCRETE_READER:BOOL=OFF

    -DSMTK_USE_SYSTEM_MOAB:BOOL=${moab_enabled}
    -DMOAB_INCLUDE_DIR:PATH=<INSTALL_DIR>/include

    # MOAB bits
    -DENABLE_HDF5:BOOL=${hdf5_enabled}
    -DENABLE_NETCDF:BOOL=${netcdf_enabled}
    -DNETCDF_ROOT:PATH=<INSTALL_DIR>

    # GDAL bits to properly enable gdal classes ( mainly wrapper )
    # that we need to build
    -DGDAL_DIR:PATH=<INSTALL_DIR>

    # Cumulus bits
    -DSMTK_ENABLE_CUMULUS_SUPPORT:BOOL=${cumulus_enabled}

    -DSMTK_FIND_SHIBOKEN:STRING=

    # Set CMAKE_INSTALL_LIBDIR to "lib" for all projects in the superbuild to
    # override OS-specific libdirs that GNUInstallDirs.cmake would otherwise
    # set.
    -DCMAKE_INSTALL_LIBDIR:STRING=lib

    ${smtk_response_file})

if (WIN32)
  set(smtk_cmakedir bin/cmake)
else ()
  set(smtk_cmakedir lib/cmake)
endif ()

superbuild_add_extra_cmake_args(
  -DSMTK_DIR:PATH=<INSTALL_DIR>/${smtk_cmakedir}/SMTK)

if ((CMAKE_CXX_COMPILER_ID STREQUAL "AppleClang" AND NOT CMAKE_CXX_COMPILER_VERSION VERSION_LESS "7.0") OR
    (CMAKE_CXX_COMPILER_ID STREQUAL "Clang" AND NOT CMAKE_CXX_COMPILER_VERSION VERSION_LESS "3.5"))
 superbuild_append_flags(cxx_flags "-Wno-inconsistent-missing-override" PROJECT_ONLY)
endif ()

set(smtk_can_lfs FALSE)
set(smtk_lfs_depends)
if (smtk_SOURCE_SELECTION STREQUAL "git")
  set(smtk_can_lfs TRUE)
elseif (smtk_SOURCE_SELECTION STREQUAL "source")
  if (EXISTS "${smtk_SOURCE_DIR}/.git")
    set(smtk_can_lfs TRUE)
  endif ()
elseif (smtk_SOURCE_SELECTION STREQUAL "from_cmb")
  if (EXISTS "${smtk_source_dir}/ThirdParty/SMTK/.git" OR
      cmb_SOURCE_SELECTION STREQUAL "git")
    set(smtk_can_lfs TRUE)
  endif ()
  if (cmb_SOURCE_SELECTION STREQUAL "git")
    # This is unfortunately necessary since the custom command is generated
    # deep in the common-superbuild/ExternalProject infrastructure. A more
    # proper solution would be to get the stamp file for each step and use
    # `add_custom_command(APPEND)` to amend the dependencies for the `lfs-init`
    # step to depend on the stamp file for the download step of CMB, but since
    # CMB's target has not yet been declared, we can't query it here and the
    # custom command doesn't exist at the top-level (where other `from_cmb`
    # logic exists.
    list(APPEND smtk_lfs_depends
      "${CMAKE_BINARY_DIR}/superbuild/cmb/stamp/cmb-download")
  endif ()
endif ()

option(smtk_FETCH_LFS "Fetch LFS data for CMB" OFF)
if (smtk_enabled AND smtk_can_lfs AND smtk_FETCH_LFS)
  cmb_superbuild_add_lfs_steps(smtk ${smtk_lfs_depends})
endif ()
