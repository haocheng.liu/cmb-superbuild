set(shiboken_extra_args)
if (UNIX AND NOT APPLE)
  list(APPEND shiboken_extra_args
    -DPYTHON_SITE_PACKAGES:STRING=lib/python2.7/site-packages)
elseif (WIN32)
  list(APPEND shiboken_extra_args
    -DPYTHON_SITE_PACKAGES:STRING=bin/Lib/site-packages)
endif ()

superbuild_add_project(shiboken
  DEPENDS qt python
  DEPENDS_OPTIONAL qt4 qt5
  CMAKE_ARGS
    -DSET_RPATH:BOOL=ON
    -DBUILD_SHARED_LIBS:BOOL=ON
    -DDISABLE_DOCSTRINGS:BOOL=ON
    -DBUILD_TESTS:BOOL=OFF
    -DSHIBOKEN_QT_VERSION:STRING=${qt_version}
    ${shiboken_extra_args})

if (NOT APPLE)
  superbuild_apply_patch(shiboken pythonpath
    "Fix relative python path settings")
endif ()
