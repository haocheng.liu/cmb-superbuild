set(CPACK_PACKAGE_DESCRIPTION_SUMMARY
  "CMB Tools for Hydrologigal Simulations")
set(CPACK_PACKAGE_NAME "CMB")
set(cmb_package_name "CMB-Hydro")

set(cmb_programs_to_install
  #GeologyBuilder
  GrabCuts
  SceneBuilder
  ModelBuilder
  MeshViewer
  PointsBuilder
  paraview
  TemplateEditor)

set(cmb_install_paraview_server TRUE)
set(cmb_install_paraview_python TRUE)

include(cmb.bundle.common)

if (opencv_enabled)
  list(APPEND cmb_python_modules
    cv2)
endif ()
