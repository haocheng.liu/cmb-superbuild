# Consolidates platform independent stub for cmb.bundle.cmake files.

include(cmb-version)
include(paraview-version)

set(CPACK_PACKAGE_VENDOR "Kitware, Inc.")
set(CPACK_PACKAGE_VERSION_MAJOR ${cmb_version_major})
set(CPACK_PACKAGE_VERSION_MINOR ${cmb_version_minor})
set(CPACK_PACKAGE_VERSION_PATCH ${cmb_version_patch}${cmb_version_suffix})
if (CMB_PACKAGE_SUFFIX)
  set(CPACK_PACKAGE_VERSION_PATCH ${CPACK_PACKAGE_VERSION_PATCH}-${CMB_PACKAGE_SUFFIX})
endif ()

if (NOT DEFINED package_filename)
  set(package_filename "${CMB_PACKAGE_FILE_NAME}")
endif ()

if (package_filename)
  set(CPACK_PACKAGE_FILE_NAME "${package_filename}")
else ()
  set(CPACK_PACKAGE_FILE_NAME
    "${cmb_package_name}-${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH}")
endif ()

# Set the license file.
set(CPACK_RESOURCE_FILE_LICENSE
    "${superbuild_source_directory}/License.txt")

list(SORT cmb_programs_to_install)
list(REMOVE_DUPLICATES cmb_programs_to_install)

function (cmb_add_plugin output)
  set(contents "<?xml version=\"1.0\"?>\n<Plugins>\n</Plugins>\n")
  foreach (name IN LISTS ARGN)
    set(plugin_directive "  <Plugin name=\"${name}\" auto_load=\"1\" />\n")
    string(REPLACE "</Plugins>" "${plugin_directive}</Plugins>" contents "${contents}")
  endforeach ()
  file(WRITE "${output}" "${contents}")
endfunction ()

set(paraview_executables)
if (cmb_install_paraview_server)
  list(APPEND paraview_executables
    pvserver
    pvdataserver
    pvrenderserver)
endif ()
if (cmb_install_paraview_python)
  list(APPEND paraview_executables
    pvbatch
    pvpython)
endif ()

set(cmb_plugins_smtk
  smtkDelaunayPlugin
  smtkDiscreteSessionPlugin
  smtkExodusSessionPlugin
  smtkMeshSessionPlugin
  smtkOpenCVPlugin
  smtkPQOperatorViewsPlugin
  smtkPolygonSessionPlugin
  smtkRemoteSessionPlugin
  smtkRemusMeshOperatorPlugin
  smtkVTKOperatorsPlugin
)

if (cgm_enabled)
  list(APPEND cmb_plugins_smtk
    smtkCGMSessionPlugin)
endif ()

if (matplotlib_enabled)
  list(APPEND cmb_plugins_smtk
    smtkMatplotlibPlugin)
endif ()

if (vxl_enabled)
  list(APPEND cmb_plugins_smtk
    smtkVXLOperatorViewsPlugin)
endif ()

set(cmb_plugins_cmb
  CMB_Plugin
  ModelBridge_Plugin)

set(cmb_plugins_all
  ${cmb_plugins_smtk}
  ${cmb_plugins_cmb})

set(cmb_plugins_ModelBuilder
  #${cmb_plugins_cmb} # Autoloaded
  ${cmb_plugins_smtk})
set(cmb_plugins_paraview
  CMB_Plugin)

set(cmb_python_modules
  smtk
  shiboken
  paraview
  cinema_python
  pygments
  six
  vtk)

if (matplotlib_enabled)
  list(APPEND cmb_python_modules
    matplotlib)
endif ()

if (numpy_enabled)
  list(APPEND cmb_python_modules
    numpy)
endif ()

if (pythongirderclient_enabled)
  list(APPEND cmb_python_modules
    requests
    girder_client)
endif ()

if (paraviewweb_enabled)
  list(APPEND cmb_python_modules
    autobahn
    constantly
    incremental
    twisted
    wslink
    zope)

  if (WIN32)
    list(APPEND cmb_python_modules
      adodbapi
      isapi
      pythoncom
      win32com)
  endif ()
endif ()

if (qt5_enabled)
  include(qt5.functions)

  set(qt5_plugin_prefix)
  if (NOT WIN32)
    set(qt5_plugin_prefix "lib")
  endif ()

  set(qt5_plugins
    sqldrivers/${qt5_plugin_prefix}qsqlite)

  if (WIN32)
    list(APPEND qt5_plugins
      platforms/qwindows)
  elseif (APPLE)
    list(APPEND qt5_plugins
      platforms/libqcocoa
      printsupport/libcocoaprintersupport)
  elseif (UNIX)
    list(APPEND qt5_plugins
      platforms/libqxcb
      platforminputcontexts/libcomposeplatforminputcontextplugin
      xcbglintegrations/libqxcb-glx-integration)
  endif ()

  superbuild_install_qt5_plugin_paths(qt5_plugin_paths ${qt5_plugins})
else ()
  set(qt5_plugin_paths)
endif ()

function (cmb_install_pdf project filename)
  if (${project}_enabled)
    install(
      FILES       "${superbuild_install_location}/doc/${filename}"
      DESTINATION "${cmb_doc_dir}"
      COMPONENT   superbuild)
  endif ()
endfunction ()

function (cmb_install_extra_data)
  if (cmb_doc_dir)
    cmb_install_pdf(cmbusersguide "CMBUsersGuide.pdf")
    cmb_install_pdf(smtkusersguide "SMTKUsersGuide.pdf")
  endif ()
endfunction ()
