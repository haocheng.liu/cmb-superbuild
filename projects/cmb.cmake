set(cmb_extra_optional_dependencies)
if (USE_NONFREE_COMPONENTS)
  list(APPEND cmb_extra_optional_dependencies
    triangle)
endif ()

set(cmb_test_plugin_dir lib)
if (WIN32)
  set(cmb_test_plugin_dir bin)
endif()

set(cmb_response_file)
if (WIN32)
  # Force response file usage in CMB. The command line gets way too long on
  # Windows without this. Once VTK_USE_FILE and PARAVIEW_USE_FILE are gone,
  # this can be removed again.
  set(cmb_response_file -DCMAKE_NINJA_FORCE_RESPONSE_FILE:BOOL=ON)
endif ()

set(cmb_extra_cmake_args)
if (UNIX AND NOT APPLE)
  list(APPEND cmb_extra_cmake_args
    -DCMAKE_INSTALL_RPATH_USE_LINK_PATH:BOOL=FALSE)
endif ()

get_property(cmb_lfs_steps GLOBAL
  PROPERTY cmb_superbuild_lfs_steps)

superbuild_add_project(cmb
  DEVELOPER_MODE
  DEBUGGABLE
  DEFAULT_ON
  INDEPENDENT_STEP_TARGETS ${cmb_lfs_steps} download update
  DEPENDS boost remus vxl gdal nlohmannjson qt python paraview
          cmbworkflows cmbusersguide smtkusersguide zeromq opencv
  DEPENDS_OPTIONAL moab smtk cgm cumulus ${cmb_extra_optional_dependencies}
                   cxx11 gdal hdf5 netcdf opencv pybind11 qt4 qt5 shiboken
  CMAKE_ARGS
    ${cmb_extra_cmake_args}
    -DKML_DIR:PATH=<INSTALL_DIR>
    -DGDAL_DIR:PATH=<INSTALL_DIR>
    -DCMB_TEST_DATA_ROOT:PATH=${CMB_TEST_DATA_ROOT}
    -DPARAVIEW_QT_VERSION:STRING=${qt_version}

    #specify semi-colon separated paths for session plugins
    -DCMB_TEST_PLUGIN_PATHS:STRING=<INSTALL_DIR>/${cmb_test_plugin_dir}
    #specify what mesh workers we should build
    -DBUILD_TRIANGLE_MESH_WORKER:BOOL=${triangle_enabled}

    # specify the apple app install prefix. No harm in specifying it for all
    # platforms.
    -DMACOSX_APP_INSTALL_PREFIX:PATH=<INSTALL_DIR>/Applications

    # This should be off by default because vtkCmbMoabReader in discrete
    # session may only be needed for debugging purpose
    -DSMTK_ENABLE_MOAB_DISCRETE_READER:BOOL=OFF

    -DSMTK_ENABLE_OPENCV:BOOL=${opencv_enabled}
    -DSMTK_ENABLE_QT_SUPPORT:BOOL=ON
    -DSMTK_ENABLE_VTK_SUPPORT:BOOL=ON
    -DSMTK_ENABLE_PARAVIEW_SUPPORT:BOOL=ON
    -DSMTK_ENABLE_CGM_SESSION:BOOL=${cgm_enabled}
    -DSMTK_ENABLE_DISCRETE_SESSION:BOOL=ON
    -DSMTK_ENABLE_EXODUS_SESSION:BOOL=ON
    -DSMTK_ENABLE_REMOTE_SESSION:BOOL=ON
    -DSMTK_ENABLE_REMUS_SUPPORT:BOOL=ON
    -DSMTK_ENABLE_PYTHON_WRAPPING:BOOL=${smtk_enable_python_wrapping}
    -DSMTK_ENABLE_VXL_SUPPORT:BOOL=${vxl_enabled}
    -DSMTK_USE_PYBIND11:BOOL=${pybind11_enabled}
    -DSMTK_QT_VERSION:STRING=${qt_version}
    -DSMTK_DATA_DIR:PATH=${CMB_TEST_DATA_ROOT}

    -DSMTK_USE_SYSTEM_MOAB:BOOL=${moab_enabled}
    -DMOAB_INCLUDE_DIR:PATH=<INSTALL_DIR>/include

    # SMTK bits
    -DUSE_SYSTEM_SMTK:BOOL=${smtk_enabled}
    -DENABLE_HDF5:BOOL=${hdf5_enabled}
    -DENABLE_NETCDF:BOOL=${netcdf_enabled}
    -DNETCDF_ROOT:PATH=<INSTALL_DIR>

    # Cumulus bits
    -DSMTK_ENABLE_CUMULUS_SUPPORT:BOOL=${cumulus_enabled}
    -DENABLE_Cumulus:BOOL=${cumulus_enabled}

    -DCMB_SUPERBUILD_DEVELOPER_ROOT:PATH=<INSTALL_DIR>

    # Set CMAKE_INSTALL_LIBDIR to "lib" for all projects in the superbuild to
    # override OS-specific libdirs that GNUInstallDirs.cmake would otherwise
    # set.
    -DCMAKE_INSTALL_LIBDIR:STRING=lib

    ${cmb_response_file})

if ((CMAKE_CXX_COMPILER_ID STREQUAL "AppleClang" AND NOT CMAKE_CXX_COMPILER_VERSION VERSION_LESS "7.0") OR
    (CMAKE_CXX_COMPILER_ID STREQUAL "Clang" AND NOT CMAKE_CXX_COMPILER_VERSION VERSION_LESS "3.5"))
 superbuild_append_flags(cxx_flags "-Wno-inconsistent-missing-override" PROJECT_ONLY)
endif ()

set(cmb_can_lfs FALSE)
if (cmb_SOURCE_SELECTION STREQUAL "git")
  set(cmb_can_lfs TRUE)
elseif (cmb_SOURCE_SELECTION STREQUAL "source")
  if (EXISTS "${cmb_SOURCE_DIR}/.git")
    set(cmb_can_lfs TRUE)
  endif ()
endif ()

option(cmb_FETCH_LFS "Fetch LFS data for CMB" OFF)
if (cmb_enabled AND cmb_can_lfs AND cmb_FETCH_LFS)
  cmb_superbuild_add_lfs_steps(cmb)
endif ()
