set(paraview_extra_cmake_args)
if (PV_NIGHTLY_SUFFIX)
  list(APPEND paraview_extra_cmake_args
    -DPV_NIGHTLY_SUFFIX:STRING=${PV_NIGHTLY_SUFFIX})
endif ()

set(paraview_disable_plugins
  EyeDomeLighting
  SciberQuestToolKit
  PointSprite
  NonOrthogonalSource
  PacMan
  StreamingParticles
  SierraPlotTools
  SLACTools
  UncertaintyRendering
  SurfaceLIC
  EyeDomeLighting
  RGBZView
  MobileRemoteControl)
foreach (plugin IN LISTS paraview_disable_plugins)
  list(APPEND paraview_extra_cmake_args
    -DPARAVIEW_BUILD_PLUGIN_${plugin}:BOOL=FALSE)
endforeach ()

if (APPLE)
  list(APPEND paraview_extra_cmake_args
    # We are having issues building mpi4py with Python 2.6 on Mac OSX. Hence,
    # disable it for now.
    -DPARAVIEW_USE_SYSTEM_MPI4PY:BOOL=ON)
endif ()

if (UNIX AND NOT APPLE)
  list (APPEND paraview_extra_cmake_args
    -DCMAKE_INSTALL_RPATH_USE_LINK_PATH:BOOL=FALSE)
endif ()

if (__BUILDBOT_INSTALL_LOCATION)
  list(APPEND paraview_extra_cmake_args
    -DPARAVIEW_CUSTOM_INSTALL_NAME_DIR:STRING=<INSTALL_DIR>
    -DPARAVIEW_DO_UNIX_STYLE_INSTALLS:BOOL=ON)
endif ()

list(APPEND paraview_extra_cmake_args
  -DPARAVIEW_EXTRA_EXTERNAL_PLUGINS:STRING=CMB_Plugin)

#this can't be quoted, since that will introduce an extra
#set of quotes into pqparaviewInitializer, and break the build
set(paraview_optional_plugins CMB_Plugin${_superbuild_list_separator}KMLExporter_Plugin)

superbuild_add_project(paraview
  DEBUGGABLE
  DEPENDS
    boost
    freetype
    gdal
    png
    python
    qt
    zlib
    netcdf
  DEPENDS_OPTIONAL
    cxx11 hdf5 paraviewweb qt4 qt5
  CMAKE_ARGS
    -DBUILD_SHARED_LIBS:BOOL=ON
    -DBUILD_TESTING:BOOL=OFF
    -DPARAVIEW_BUILD_PLUGIN_CoProcessingScriptGenerator:BOOL=ON
    -DPARAVIEW_BUILD_QT_GUI:BOOL=${qt_enabled}
    -DPARAVIEW_QT_VERSION:STRING=${qt_version}
    -DPARAVIEW_ENABLE_PYTHON:BOOL=${python_enabled}
    -DPARAVIEW_ENABLE_WEB:BOOL=OFF
    -DPARAVIEW_USE_MPI:BOOL=${mpi_enabled}
    -DVTK_USE_SYSTEM_HDF5:BOOL=${hdf5_enabled}
    -DHDF5_NO_FIND_PACKAGE_CONFIG_FILE:BOOL=ON
    -DVTK_USE_SYSTEM_NETCDF:BOOL=${netcdf_enabled}
    -DVTK_RENDERING_BACKEND:STRING=OpenGL2
    -DPARAVIEW_ENABLE_WEB:BOOL=${paraviewweb_enabled}
    -DPARAVIEW_USE_ICE_T:BOOL=ON

    #currently catalyst is having problems on praxis so lets disable it for now
    -DPARAVIEW_ENABLE_CATALYST:BOOL=OFF

    #CMB needs geovis enabled to provide the gdal reader
    -DModule_vtkGeovisCore:BOOL=ON
    -DModule_vtkIOGDAL:BOOL=ON
    -DModule_vtkViewsInfovis:BOOL=ON
    -DModule_vtkRenderingMatplotlib:BOOL=ON
    -DModule_vtkRenderingGL2PSOpenGL2:BOOL=ON
    -DGDAL_DIR:PATH=<INSTALL_DIR>

    # CMB needs to specify external plugins so that we can let paraview
    # properly install the plugins. So we sneakily force a variable that is an
    # implementation detail of paraview branding
    -DBPC_OPTIONAL_PLUGINS:INTERNAL=${paraview_optional_plugins}
    -DPARAVIEW_INSTALL_DEVELOPMENT_FILES:BOOL=TRUE
    # since VTK mangles all the following, I wonder if there's any point in
    # making it use system versions.
    -DVTK_USE_SYSTEM_FREETYPE:BOOL=${freetype_enabled}
#    currently png strips rpaths don't use this in cmb, so don't use this
#    -DVTK_USE_SYSTEM_PNG:BOOL=${png_enabled}
    -DVTK_USE_SYSTEM_ZLIB:BOOL=${zlib_enabled}

    # Specify the apple app install prefix. No harm in specifying it for all
    # platforms.
    -DMACOSX_APP_INSTALL_PREFIX:PATH=<INSTALL_DIR>/Applications

    #If this is true paraview doesn't properly clean the paths to system
    #libraries like netcdf
    -DCMAKE_INSTALL_RPATH_USE_LINK_PATH:BOOL=TRUE
    -DCMAKE_MACOSX_RPATH:BOOL=FALSE

    # Keep up with the latest ParaView & VTK by avoiding the use of legacy API
    -DVTK_LEGACY_REMOVE:BOOL=TRUE

    ${paraview_extra_cmake_args})

if (__BUILDBOT_INSTALL_LOCATION)
  set(paraview_paraview_dir <INSTALL_DIR>/lib/cmake/paraview-${paraview_version})
  set(paraview_vtk_dir ${paraview_paraview_dir})
else ()
  set(paraview_paraview_dir ${CMAKE_CURRENT_BINARY_DIR}/paraview/build)
  set(paraview_vtk_dir ${paraview_paraview_dir}/VTK)
endif ()

superbuild_add_extra_cmake_args(
  -DParaView_DIR:PATH=${paraview_paraview_dir}
  -DVTK_DIR:PATH=${paraview_vtk_dir})
