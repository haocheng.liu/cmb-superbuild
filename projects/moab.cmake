superbuild_add_project(moab
  DEPENDS hdf5 netcdf
  DEPENDS_OPTIONAL cxx11
                   #cgm
  CMAKE_ARGS
    -DBUILD_SHARED_LIBS:BOOL=OFF
    -DMOAB_USE_SZIP:BOOL=ON
    -DMOAB_USE_CGM:BOOL=OFF
    -DMOAB_USE_CGNS:BOOL=OFF
    -DMOAB_USE_MPI:BOOL=OFF
    -DMOAB_USE_HDF:BOOL=ON
    -DENABLE_HDF5:BOOL=ON # also required to get hdf5 support enabled
    -DMOAB_USE_NETCDF:BOOL=ON
    -DNETCDF_ROOT:PATH=<INSTALL_DIR>
    -DMOAB_ENABLE_TESTING:BOOL=ON) # build can't handle this being disabled
