set(CPACK_MONOLITHIC_INSTALL TRUE)

# URL to website providing assistance in installing your application.
set(CPACK_NSIS_HELP_LINK "https://gitlab.kitware.com/cmb/cmb/wikis/home")

#FIXME: need a pretty icon.
#set(CPACK_NSIS_MUI_ICON "${CMAKE_CURRENT_LIST_DIR}/paraview.ico")

set(ModelBuilder_description "Model Builder")
set(paraview_description "ParaView")
set(pvserver_description "ParaView (server)")
set(pvdataserver_description "ParaView (data server)")
set(pvrenderserver_description "ParaView (render server)")
set(pvpython_description "ParaView (Python shell)")

set(library_paths "lib")

if (USE_SYSTEM_qt4)
  list(APPEND library_paths
    "${QT_LIBRARY_DIR}")
endif ()

if (Qt5_DIR)
  list(APPEND library_paths
    "${Qt5_DIR}/../../../bin")
endif ()

if (opencv_enabled)
  if (MSVC12)
    set(msvc_ver vc12)
  elseif (MSVC14)
    set(msvc_ver vc14)
  else ()
    message(FATAL_ERROR "Unrecognized MSVC version")
  endif ()

  if (superbuild_is_64bit)
    set(msvc_arch x64)
  else ()
    set(msvc_arch x86)
  endif ()

  list(APPEND library_paths
    "${superbuild_install_location}/${msvc_arch}/${msvc_ver}/bin")
endif ()

if (matplotlib_enabled)
  install(
    DIRECTORY   "${superbuild_install_location}/bin/Lib/site-packages/matplotlib/mpl-data/"
    DESTINATION "bin/Lib/site-packages/matplotlib/mpl-data"
    COMPONENT   superbuild)
endif ()

set(plugins)
foreach (executable IN LISTS paraview_executables cmb_programs_to_install)
  if (DEFINED "${executable}_description")
    list(APPEND CPACK_NSIS_MENU_LINKS
      "bin/${executable}.exe" "${${executable}_description}")
  else ()
    message(WARNING "No description for ${executable} given.")
  endif ()

  superbuild_windows_install_program("${executable}"
    "${library_paths}")
  list(APPEND plugins
    ${cmb_plugins_${executable}})
endforeach ()

list(REMOVE_DUPLICATES plugins)

foreach (plugin IN LISTS plugins)
  superbuild_windows_install_plugin("${plugin}.dll"
    "bin"
    "${library_paths}")
endforeach ()

superbuild_windows_install_python(
  MODULES ${cmb_python_modules}
  MODULE_DIRECTORIES  "${superbuild_install_location}/bin/Lib/site-packages"
                      "${superbuild_install_location}/lib/site-packages"
                      "${superbuild_install_location}/lib/paraview-${paraview_version}/site-packages"
  SEARCH_DIRECTORIES  "${library_paths}")

superbuild_windows_install_python(
  "${CMAKE_INSTALL_PREFIX}"
  MODULES vtk
  NAMESPACE "/paraview"
  MODULE_DIRECTORIES  "${superbuild_install_location}/bin/Lib/site-packages"
                      "${superbuild_install_location}/lib/site-packages"
                      "${superbuild_install_location}/lib/paraview-${paraview_version}/site-packages"
  SEARCH_DIRECTORIES  "${library_paths}")

include(python.functions)
superbuild_install_superbuild_python()

if (pythonrequests_enabled)
  install(
    FILES       "${superbuild_install_location}/bin/Lib/site-packages/requests/cacert.pem"
    DESTINATION "bin/Lib/site-packages/requests"
    COMPONENT   superbuild)
endif ()

if (paraviewweb_enabled)
  install(
    DIRECTORY   "${superbuild_install_location}/bin/Lib/site-packages/win32"
    DESTINATION "bin/Lib/site-packages"
    COMPONENT   "superbuild")
  install(
    FILES       "${superbuild_install_location}/bin/Lib/site-packages/pywin32.pth"
                "${superbuild_install_location}/bin/Lib/site-packages/pywin32.version.txt"
    DESTINATION "bin/Lib/site-packages"
    COMPONENT   "superbuild")

  install(
    FILES       "${superbuild_install_location}/lib/paraview-${paraview_version}/site-packages/paraview/web/defaultProxies.json"
    DESTINATION "bin/Lib/site-packages/paraview/web"
    COMPONENT   "superbuild")
  install(
    DIRECTORY   "${superbuild_install_location}/share/paraview/web"
    DESTINATION "share/paraview-${paraview_version}"
    COMPONENT   "superbuild")
endif ()

set(plugins_file "${CMAKE_CURRENT_BINARY_DIR}/.plugins")
cmb_add_plugin("${plugins_file}" ${plugins})

install(
  FILES       "${plugins_file}"
  DESTINATION "bin"
  COMPONENT   superbuild)

install(
  DIRECTORY   "${superbuild_install_location}/share/cmb/workflows/"
  DESTINATION "Simulation Templates"
  COMPONENT   superbuild)

foreach (qt5_plugin_path IN LISTS qt5_plugin_paths)
  get_filename_component(qt5_plugin_group "${qt5_plugin_path}" DIRECTORY)
  get_filename_component(qt5_plugin_group "${qt5_plugin_group}" NAME)

  superbuild_windows_install_plugin(
    "${qt5_plugin_path}"
    "bin/${qt5_plugin_group}"
    "${library_paths}")
endforeach ()

if (qt5_enabled)
  foreach (qt5_opengl_lib IN ITEMS opengl32sw libEGL libGLESv2)
    superbuild_windows_install_plugin(
      "${Qt5_DIR}/../../../bin/${qt5_opengl_lib}.dll"
      "bin"
      "${library_paths}")
  endforeach ()
endif ()
