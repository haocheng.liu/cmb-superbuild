include("${CMAKE_CURRENT_LIST_DIR}/../shiboken.cmake")

superbuild_apply_patch(shiboken VC2015
  "Add flag to ignore deprecation warnings for VC2015.")
