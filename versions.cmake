superbuild_set_revision(boost
  URL     "http://www.paraview.org/files/dependencies/boost_1_64_0.tar.bz2"
  URL_MD5 93eecce2abed9d2442c9676914709349)

# XXX: When updating this, update the version number in CMakeLists.txt as well.
# The current version of ParaView is master branch as of October 13 2017

set(paraview_revision "6c6852c1c90c2cd3d0c2f8afc1a142b12d9acea1")
#set(paraview_revision origin/master)

if (USE_PARAVIEW_master)
  set(paraview_revision origin/master)
endif ()
superbuild_set_revision(paraview
  GIT_REPOSITORY "https://gitlab.kitware.com/paraview/paraview.git"
  GIT_TAG        "${paraview_revision}")

superbuild_set_revision(vtkonly
  GIT_REPOSITORY "https://gitlab.kitware.com/vtk/vtk.git"
  GIT_TAG        origin/master)

superbuild_set_revision(shiboken
  # https://github.com/OpenGeoscience/shiboken.git
  URL     "http://www.paraview.org/files/dependencies/shiboken-110e45fa9d873afea4d7ae47da3fb678b1831a21.tar.bz2"
  URL_MD5 53e71b32964b5daf38b45e1679623e48)

superbuild_set_selectable_source(cmb
  SELECT 4.2.0
    GIT_REPOSITORY  "https://gitlab.kitware.com/cmb/cmb.git"
    GIT_TAG         "v4.2.0"
  SELECT git CUSTOMIZABLE DEFAULT
    GIT_REPOSITORY  "https://gitlab.kitware.com/cmb/cmb.git"
    GIT_TAG         "origin/master"
  SELECT source CUSTOMIZABLE
    SOURCE_DIR "source-cmb")

set(smtk_source_dir)
if (cmb_SOURCE_SELECTION STREQUAL "source")
  set(smtk_source_dir
    "${cmb_SOURCE_DIR}")
else ()
  set(smtk_source_dir
    "${CMAKE_BINARY_DIR}/superbuild/cmb/src")
  set_property(GLOBAL
    PROPERTY
      smtk_need_cmb_download TRUE)
endif ()

superbuild_set_selectable_source(smtk
  SELECT 1.1.0
    GIT_REPOSITORY  "https://gitlab.kitware.com/cmb/smtk.git"
    GIT_TAG         "v1.1.0"
  SELECT git CUSTOMIZABLE
    GIT_REPOSITORY  "https://gitlab.kitware.com/cmb/smtk.git"
    GIT_TAG         "origin/master"
  SELECT from_cmb DEFAULT
    DOWNLOAD_COMMAND
      "${CMAKE_COMMAND}" -E echo_append
    SOURCE_DIR    "${smtk_source_dir}/ThirdParty/SMTK"
  SELECT source CUSTOMIZABLE
    SOURCE_DIR "source-smtk")

superbuild_set_revision(cmbworkflows
  GIT_REPOSITORY "https://gitlab.kitware.com/cmb/simulation-workflows.git"
  GIT_TAG        origin/master)

superbuild_set_revision(vxl
  # https://github.com/judajake/vxl.git
  URL     "http://www.paraview.org/files/dependencies/vxl-44433e4bd8ca3eabe4e5441444bf2a050d689d45.tar.bz2"
  URL_MD5 dfff6d1958334cbbadb4edbf8c3f4cb6)

# Use opencv from Thu Oct 6 13:40:33 2016 +0000
superbuild_set_revision(opencv
  # https://github.com/opencv/opencv.git
  URL     "http://www.computationalmodelbuilder.org/files/dependencies/opencv-dd379ec9fddc1a1886766cf85844a6e18d38c4f1.tar.bz2"
  URL_MD5 19bbd14ed1bd741beccd6d19e444552f)

# Use the tweaked cmake build of zeromq
superbuild_set_revision(zeromq
  # https://github.com/robertmaynard/zeromq4-x.git
  URL     "http://www.computationalmodelbuilder.org/files/dependencies/zeromq4-6d787cf69da6c69550e85a45be1bee1eb0e1c415.tar.bz2"
  URL_MD5 26790786e01a1732a8acf723b99712ba)

# Use remus from Fri Sep 1 15:56:16 2017 +0000
superbuild_set_revision(remus
  GIT_REPOSITORY "https://gitlab.kitware.com/cmb/remus.git"
  GIT_TAG        85355887b0da9d8b81bf1de999ba3a0f3ea7eb80)

superbuild_set_revision(gdal
  # https://github.com/judajake/gdal-svn.git
  URL     "http://www.computationalmodelbuilder.org/files/dependencies/gdal-98353693d6f1d607954220b2f8b040375e3d1744.tar.bz2"
  URL_MD5 5aa285dcc856f98ce44020ae1ae192cb)

superbuild_set_revision(moab
  # https://gitlab.kitware.com/third-party/moab.git
  URL     "http://www.computationalmodelbuilder.org/files/dependencies/moab-17bf5518c4e6585a824edf06bc31902baf9ad045.tar.bz2"
  URL_MD5 c1dd28514fc567b6e42acc12c2381ca4)

superbuild_set_revision(triangle
  # https://github.com/robertmaynard/triangle.git
  URL     "http://www.paraview.org/files/dependencies/triangle-4c20820448cdfa27f968cfd7cb33ea5b9426ad91.tar.bz2"
  URL_MD5 9a016bc90f1cdff441c75ceb53741b11)

superbuild_set_revision(pythonsix
  URL     "https://pypi.python.org/packages/source/s/six/six-1.10.0.tar.gz"
  URL_MD5 34eed507548117b2ab523ab14b2f8b55)

superbuild_set_revision(pythonrequests
  URL     "https://pypi.python.org/packages/source/r/requests/requests-2.9.1.tar.gz"
  URL_MD5 0b7f480d19012ec52bab78292efd976d)

superbuild_set_revision(pythonsetuptools
  URL     "https://pypi.python.org/packages/45/5e/79ca67a0d6f2f42bfdd9e467ef97398d6ad87ee2fa9c8cdf7caf3ddcab1e/setuptools-23.0.0.tar.gz"
  URL_MD5 100a90664040f8ff232fbac02a4c5652)

superbuild_set_revision(pythongirderclient
  URL     "https://pypi.python.org/packages/source/g/girder-client/girder-client-1.1.2.tar.gz"
  URL_MD5 4cd5e0cab41337a41f45453d25193dcf)

superbuild_set_revision(pybind11
  # https://github.com/pybind/pybind11.git
  URL     "http://www.paraview.org/files/dependencies/pybind11-3dde6ddc5348d5ccd006b2ce00302ed076f59454.tar.bz2"
  URL_MD5 102dbdaaa92e2ad907000e8817194353)

superbuild_set_revision(ftgl
  # https://github.com/ulrichard/ftgl.git
  URL     "http://www.paraview.org/files/dependencies/ftgl-dfd7c9f0dee7f0059d5784f3a71118ae5c0afff4.tar.bz2"
  URL_MD5 16e54c7391f449c942f3f12378db238f)

superbuild_set_revision(oce
  # https://github.com/mathstuf/oce.git
  URL     "http://www.paraview.org/files/dependencies/oce-9b4646a11c7d9be65a6c2df0ade6604cc91b1fa4.tar.bz2"
  URL_MD5 adea5bd5a7510c7da58755ca7d964319)

superbuild_set_revision(cgm
  # https://bitbucket.org/mathstuf/cgm.git
  URL     "http://www.computationalmodelbuilder.org/files/dependencies/cgm-3191a7219e16491ebe098159598ddfe7f039bffc.tar.bz2"
  URL_MD5 bf280f3bc5970c98960da9a66da2ab94)

superbuild_set_revision(paraviewwebvisualizer
  URL     "http://www.paraview.org/files/dependencies/visualizer-2.0.12.tar.gz"
  URL_MD5 56e7e241ea6ad66b44469fc3186f47d6)

superbuild_set_revision(paraviewweblightviz
  URL     "http://www.paraview.org/files/dependencies/light-viz-1.16.1.tar.gz"
  URL_MD5 9ac1937cf07ae57bf85c3240f921679a)

superbuild_set_revision(cmbusersguide
  URL     "https://media.readthedocs.org/pdf/cmb/master/cmb.pdf")

superbuild_set_revision(smtkusersguide
  URL     "https://media.readthedocs.org/pdf/smtk/latest/smtk.pdf")

superbuild_set_revision(nlohmannjson
  URL     "https://data.kitware.com/api/v1/item/59bd45638d777f7d33e9d321/download"
  URL_MD5 6b27f142b87b418b96f7c488d98dff05)
