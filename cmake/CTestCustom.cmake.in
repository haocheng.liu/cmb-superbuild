if (ctest_submit_to_smtk)
  set(CTEST_DROP_LOCATION "/submit.php?project=SMTK")
else ()
  set(CTEST_DROP_LOCATION "/submit.php?project=CMB")
endif ()

set(CTEST_CUSTOM_MAXIMUM_NUMBER_OF_ERRORS 1000)
set(CTEST_CUSTOM_MAXIMUM_NUMBER_OF_WARNINGS 1000)

##------------------------------------------------------------------------------
## Regular expression for warning exception during build process
list(APPEND CTEST_CUSTOM_WARNING_EXCEPTION
  # Ignore all warnings.
  ".*"
  "[w|W]arning"
  "WARNING"
  "CMake Warning"

  "-jN forced in submake: disabling jobserver mode"
  "thirdparty/qt5json"
  "/usr/bin/libtool"
  "/usr/bin/ranlib: file: .*/libtiff.a"
  "/usr/bin/ranlib: warning for library: .*/libvil3d_io.a"
  "/usr/bin/ranlib: warning for library: .*/libvimt_algo.a"
  "src.*smtk.*ThirdParty"
  "ld: warning: duplicate dylib"
  "paraview/build"
  "cmb/Source/Omicron"
  "/usr/bin/ranlib: file: ../../../../lib/libtl_alloc.a.* has no symbols"

  "H5LTanalyze.c"

  "has different visibility .* in .* and hidden in .*vtkInitializationHelper"
  "has different visibility .* in .* and hidden in .*CMakeFiles/pqCore.dir/"

  "build/.*/doc"

  #Shiboken
  "CMakeFiles/shiboken.dir"
  "libapiextractor.a"

  # Boost
  "clang: warning: optimization flag '-finline-functions' is not supported"
  "clang: warning: argument unused during compilation: '-finline-functions'"

  #Paraview
  "paraview/build/.*/.*/Documentation"

  #vxl
  "install/include/vxl"
  "/usr/bin/ranlib: file: .*libvcl.a"
  "/usr/bin/ranlib: file: .*libvbl.a"
  "/usr/bin/ranlib: file: .*libvsl.a"
  "/usr/bin/ranlib: file: .*libvil1.a"
  "/usr/bin/ranlib: file: .*libvil3d_io.a"
  "/usr/bin/ranlib: file: .*libvimt_algo.a"

  #windows
  "warning C4244: .* : conversion from 'Py_ssize_t' to .*, possible loss of data"
  "warning C4244: .* : conversion from 'INT_PTR' to 'int', possible loss of data"
  "src/ft2font.cpp.* : warning C4267:"
  "..\\\\Modules\\\\"
  "..\\\\Python\\\\"
  "..\\\\Parser\\\\"
  "tss_dll.obj : warning LNK4221:"
  "Modules\\\\zlib\\\\gzio.c"
  "Modules\\posixmodule.c"
  "Modules\\\\_ctypes\\\\libffi_msvc\\\\ffi.c"
  "ttconv/.*.cpp"
  "lib/matplotlib"
  "src/image.cpp"
  "include\\\\Qt.*"
  "paraview\\\\src\\\\paraview\\\\ThirdParty"
  "non dll-interface class 'google"
  "include\\\\qtcore\\\\q.*.h"
  "src\\\\smtk\\\\ThirdParty"
  "qlist.h.* : warning C4127:"
  "qvector.h.* : warning C4127:"
  "qhash.h.* : warning C4127:"
  "qglobal.h.* : warning C4512:"
  "qmap.h.* : warning C4127:"
  "qstringbuilder.h.* : warning C4512:"
  "qmutex.h.* : warning C4512:"
  "vil3d_dicom.obj : warning LNK4221:"
  "install\\\\include"
  "shiboken\\\\build"
  "smtk\\\\src\\\\smtk\\\\ThirdParty"
  "warning C4996: 'strcpy'"
  "warning C4996: 'scanf'"
  "warning C4996: 'fopen'"
  "warning C4996: 'sprintf'"
  "warning C4996: 'sscanf'"
  "warning C4996: '_snprintf'"
  "..\\\\PC\\\\_.*.c.* : warning C42"
  "..\\\\Objects\\\\obmalloc.c.* : warning C4244"
  "conversion from 'npy_intp' to 'int', possible loss of data"
  "paraview\\\\.* : warning C4996:"
  "paraview\\\\.* : warning C4512:"
  "paraview\\\\.* : warning C4127"
  "Microsoft Visual Studio .*\\\\VC\\\\INCLUDE"
  "libs\\\\filesystem\\\\src\\\\.* : warning C4244:"
  "libs\\\\filesystem\\\\src\\\\.* : warning C4267:"
  "libs\\\\thread\\\\src\\\\win32\\\\.* : warning C4267:"
  "include\\\\qtcore"
  "cmb\\\\src\\\\cmb\\\\Source\\\\Omicron"
  "warning LNK4098: defaultlib 'LIBCMT'"
  "_ttconv.obj : warning LNK4197:"
  "cntr.obj : warning LNK4197:"
  "_delaunay.obj : warning LNK4197:"
  "nxutils.obj : warning LNK4197:"
  "path.obj : warning LNK4197:"
  "backend_agg.obj : warning LNK4197:"
  "_png.obj : warning LNK4197:"
  "_windowing.obj : warning LNK4197:"
  "ParaViewCore\\\\ServerImplementation\\\\Core\\\\vtkPVMessage.pb.cc"
  "ParaViewCore\\\\ServerImplementation\\\\Core\\\\vtkPVMessage.pb.h"
  "paraview\\\\src\\\\paraview\\\\Plugins\\\\H5PartReader\\\\H5Part\\\\src\\\\H5Part.c"
  "Applications\\\\ParaView\\\\paraview_main.cxx.* : warning C4996:"
  "cmb\\\\build\\\\source\\\\omicron"
)

ignore_project_warnings(boost)
ignore_project_warnings(gdal)
ignore_project_warnings(hdf5)
ignore_project_errors(hdf5)
ignore_project_warnings(python)
ignore_project_warnings(qt4)
ignore_project_warnings(qt5)
ignore_project_warnings(shiboken)
ignore_project_warnings(szip)
ignore_project_warnings(vxl)
ignore_project_warnings(zeromq)

if (WIN32)
  set(ENV{PATH}
    "$ENV{PATH};${CMAKE_CURRENT_LIST_DIR}/install/bin;${CMAKE_CURRENT_LIST_DIR}/install/lib;${CMAKE_CURRENT_LIST_DIR}/install/x64/vc14/bin")
endif ()

# Regular expression for error exceptions during build process
list(APPEND CTEST_CUSTOM_ERROR_EXCEPTION
  #clang warning showing up as error
  "in-class initializer for static data member of type 'const double' is a GNU extension"

  # from free-type
  "install/include/freetype2/freetype/internal"

  # skip BLAS and Lapack not found errors
  "BlasNotFoundError"
  "LapackSrcNotFoundError"
)
